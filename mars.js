const cams = {
    FHAZ: 'Front Hazard Avoidance Camera',
    RHAZ: 'Rear Hazard Avoidance Camera',
    MAST: 'Mast Camera',
    CHEMCAM: 'Chemistry and Camera Complex',
    MAHLI: 'Mars Hand Lens Imager',
    MARDI: 'Mars Descent Imager',
    NAVCAM: 'Navigation Camera',
    PANCAM: 'Panoramic Camera',
    MINITES: 'Miniature Thermal Emission Spectrometer',
    EDL_DDCAM: 'Descent Stage Down-Look Camera',
    EDL_PUCAM1: 'Parachute Up-Look Camera A',
    EDL_PUCAM2: 'Parachute Up-Look Camera B',
    EDL_RDCAM: 'Rover Down-Look Camera',
    EDL_RUCAM: 'Rover Up-Look Camera',
    FRONT_HAZCAM_LEFT_A: 'Front Hazard Avoidance Camera - Left',
    FRONT_HAZCAM_RIGHT_A: 'Front Hazard Avoidance Camera - Right',
    MCZ_LEFT: 'Mast Camera Zoom - Left',
    MCZ_RIGHT: 'Mast Camera Zoom - Right',
    NAVCAM_LEFT: 'Navigation Camera - Left',
    NAVCAM_RIGHT: 'Navigation Camera - Right',
    REAR_HAZCAM_LEFT: 'Rear Hazard Avoidance Camera - Left',
    REAR_HAZCAM_RIGHT: 'Rear Hazard Avoidance Camera - Right',
    SHERLOC_WATSON: 'SHERLOC WATSON Camera',
    SKYCAM: 'MEDA Skycam'
};

const photoEl = document.getElementById('photos');
const roverEl = document.getElementById('rover-select');
const camEl = document.getElementById('cam-selector');
const solEl = document.getElementById('custom-sol');
const formEl  = document.getElementById('custom-album');
const solForm = document.getElementById('sol-form');
const albumInfo = document.getElementById('album-info');
const invalidEl = document.getElementById('invalid-msg');
const noPhoto = document.getElementById('no-photo-notice');
let roverFlag = 'perseverance';
let camFlag = 'all';
let solFlag = -1;
let maxFlag = 0;

// One or more Classes.
class MarsRover {
    constructor(rover, cam, sol) {
        this.rover = rover;
        this.cam = cam;
        this.sol = sol;
        this.maxSol = 0;
        this.photoData = {};
    }

    buildAlbum() {
        // One or more fetch requests to a 3rd party API.
        const manufestURL = `https://api.nasa.gov/mars-photos/api/v1/manifests/${this.rover}?api_key=${API_KEY}`;
        solEl.style.borderColor = '';
        invalidEl.innerText = '';
        fetch(manufestURL)
            .then(response => response.json())
            .then(data => {
                let photoList = data.photo_manifest.photos;
                this.maxSol = data.photo_manifest.max_sol;
                solEl.placeholder = `Available Sol Value: 0 - ${this.maxSol}`;
                maxFlag = this.maxSol;
                if (this.sol == -1) {
                    this.photoData = photoList[Math.floor(Math.random() * photoList.length)];
                    this.sol = this.photoData.sol;
                } else {
                    this.photoData = photoList.find(element => element.sol == this.sol);
                }
                solFlag = this.sol;
                camEl.innerHTML = `<option selected>Select Camera</option>`;
                this.photoData.cameras.forEach(camera => {
                    let camOption = document.createElement('option');
                    camOption.value = camera;
                    camOption.textContent = cams[camera];
                    camEl.appendChild(camOption);
                });
                let photoURL = '';
                if (this.cam == 'all') {
                    photoURL = `https://api.nasa.gov/mars-photos/api/v1/rovers/${this.rover}/photos?sol=${this.sol}&api_key=${API_KEY}`;
                } else {
                    photoURL = `https://api.nasa.gov/mars-photos/api/v1/rovers/${this.rover}/photos?sol=${this.sol}&camera=${this.cam}&api_key=${API_KEY}`;
                }
                return fetch(photoURL);
            })
            .then(response => response.json())
            .then(data => {
                // console.log(this.sol);
                // console.log('Input: ' + solEl.value)
                let album = data.photos;
                albumInfo.innerHTML = `
                <h2 class="text-center">Rover: <i class="text-primary">${album[0].rover.name}</i></h2>
                <h3 class="text-center">Earth Date: <i class="text-primary">${album[0].earth_date}</i></h3>
                <h3 class="text-center">Mission Day (Sol): <i class="text-primary">${album[0].sol}</i></h3>
                <h3 class="text-center">Total Photos: <i class="text-primary">${album.length}</i></h3>
                `;
                album.forEach(element => {
                    // create card element
                    let card = document.createElement('div');
                    card.className = 'card';
                    card.innerHTML = `
                    <img class="card-img-top" src="${element.img_src}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Photo ID: <i class="text-primary">${element.id}</i></h5>
                        <p class="card-text"><b>Camera: <i class="text-primary">${element.camera.full_name}</i></b></p>
                        <a href="${element.img_src}" target="_blank" class="btn btn-primary">Photo Link</a>
                    </div>
                    `;
                    photoEl.appendChild(card);
                })
            })
            .catch(error => {
                console.error('Error:', error);
                albumInfo.innerHTML = '';
                noPhoto.hidden = false;
                noPhoto.innerHTML = `<h3 class="text-center"><i>No photos on sol ${solEl.value}</i></h2>`;
                // One or more timing functions
                setTimeout(function() {
                    noPhoto.hidden = true;
                }, 5000);
            });
    }

}

roverEl.addEventListener('click', e => {
    let selected = e.target;
    switch(selected.id) {
        case 'rover-p':
            rebuildAlbum('perseverance', 'all', -1);
            roverFlag = 'perseverance';
            solEl.value = '';
            break;
        case 'rover-c':
            rebuildAlbum('curiosity', 'all', -1);
            roverFlag = 'curiosity';
            solEl.value = '';
            break;
        case 'rover-o':
            rebuildAlbum('opportunity', 'all', -1);
            roverFlag = 'opportunity';
            solEl.value = '';
            break;
        case 'rover-s':
            rebuildAlbum('spirit', 'all', -1);
            roverFlag = 'spirit';
            solEl.value = '';
            break;
        default:
            rebuildAlbum(roverFlag, 'all', -1);
            solEl.value = '';
    }
});

function rebuildAlbum (rover, cam, sol) {
    photoEl.innerHTML = '';
    let roverPhoto = new MarsRover(rover, cam, sol);
    roverPhoto.buildAlbum();  
}

formEl.addEventListener('submit', e => {
    e.preventDefault();
    const cam = camEl.value;
    rebuildAlbum(roverFlag, cam, solFlag);
});

solForm.addEventListener('click', e => {
    // Contains form fields, validates those fields
    e.preventDefault();
    const sol = solEl.value;
    if ((sol < 0) || (sol > maxFlag) || (sol == '')) {
        solEl.classList.add('invalid');
        solEl.style.borderColor = 'red';
        invalidEl.innerText = `Please enter a number between 0 - ${maxFlag}`;
    } else {
        solEl.classList.remove('invalid');
        solEl.style.borderColor = '';
        invalidEl.innerText = '';
        rebuildAlbum(roverFlag, 'all', sol);
    }
});

rebuildAlbum(roverFlag, camFlag, solFlag);
